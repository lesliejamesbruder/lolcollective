require("./bootstrap");

// When the user scrolls the page, execute myFunction
window.onscroll = function() {
    PostMenuSticky();
};

// Get the navbar
var navbar = document.getElementById("header-post-bar");

// Get the offset position of the navbar
var sticky = document.getElementById("main-header").offsetHeight;

// Add the sticky class to the navbar when you reach its scroll position. Remove "sticky" when you leave the scroll position
function PostMenuSticky() {
    if (window.pageYOffset >= sticky) {
        navbar.classList.add("sticky");
        console.log("testing 2", window.pageYOffset);
    } else {
        navbar.classList.remove("sticky");
        console.log("testing 1");
    }
}
