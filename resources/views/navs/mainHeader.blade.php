<nav id="main-header" class="navbar navbar-expand-sm navbar-light">
    <div class="container">
        <div class="w-75 w-sm-100">
            <span class="navbar-brand w-100">
                <span class="title">
                    <img src="/imgs/full-logo.png">
                </span>
            </span>
        </div>
        <div class="w-25">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
                aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse w-100" id="navbarNavDropdown">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item mr-4">
                    <a class="nav-link" href="#"><i class="fas fa-bell"></i></a>
                </li>
                <li class="nav-item mr-4">
                    <a class="nav-link" href="#"><i class="fas fa-user"></i></a>
                </li>
                <li class="nav-item mr-4">
                    <a class="nav-link" href="#"><i class="fas fa-search"></i></a>
                </li>
            </ul>
        </div>
    </div>
</nav>
