<!doctype html>
<html lang="en">

<head>
    <title>LolCollective - @yield('title')</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/extend-bs.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/colours/purple-1.css') }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
</head>

<body>
    @section('header')
    @include('navs.mainHeader')
    @show

    <div class="container d-lg-flex">

        <div class="w-100 w-lg-75 order-0">
            @yield('content')
        </div>
        <div class="w-100 w-lg-25 order-1">
            @section('sidebar')
            This is the master sidebar.
            @show
        </div>
    </div>
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/popper.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="https://kit.fontawesome.com/63952c49ff.js"></script>
</body>

</html>
