const mix = require("laravel-mix");

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js("resources/js/app.js", "public/js")
    .js("node_modules/bootstrap/dist/js/bootstrap.min.js", "public/js")
    .js("node_modules/jquery/dist/jquery.min.js", "public/js")
    .js("node_modules/popper.js/dist/popper.min.js", "public/js")
    .sass("node_modules/bootstrap/scss/bootstrap.scss", "public/css")
    .sass("resources/sass/app.scss", "public/css")
    .sass("resources/sass/extend-bs.scss", "public/css")
    .sass("resources/sass/colours/purple-1.scss", "public/css/colours");
